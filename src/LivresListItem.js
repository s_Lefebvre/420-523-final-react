import React, { Component } from 'react';
import './App.css';

export default class TodosListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false
    }
  }

  renderTitreSection() {
    const { titre, autheur } = this.props;

    const titreStyle = {
          color: this.state.isEditing ? 'red' : 'green',
          cursor: 'pointer'
        };

    if (this.state.isEditing) {
      return (
        <td>
          <form onSubmit={this.onSaveClick.bind(this)}>
            <input type="text" defaultValue={titre} ref="editInput"/>
            <input type="text" defaultValue={autheur} ref="editInputAutheur"/>
          </form>
        </td>
      )
    }

    return (
        <td style={titreStyle}>{titre}</td>
    );

  }
  
  renderAutheurSection() {
    const { autheur } = this.props;

    const autheurStyle = {
          color: this.state.isEditing ? 'red' : 'green',
          cursor: 'pointer'
        };

    if (this.state.isEditing) {
      return (null)
    }

    return (
        <td style={autheurStyle}>{autheur}</td>
    );

  }
  
  renderIdSection() {
    const { id } = this.props;

    const idStyle = {
          color: this.state.isEditing ? 'red' : 'green',
          cursor: 'pointer'
        };

    if (this.state.isEditing) {
      return (null)
    }

    return (
        <td style={idStyle}>{id}</td>
    );

  }

  renderActionSection() {
    if (this.state.isEditing) {
      return (
        <td>
          <button onClick={this.onSaveClick.bind(this)}>Save</button>
          <button onClick={this.onCancelClick.bind(this)}>Cancel</button>
        </td>
      )
    }

    return (
      <td>
        <button onClick={this.onEditClick.bind(this)}>Edit</button>
        <button onClick={this.props.deleteLivre.bind(this, this.props.livre)}>Delete</button>
      </td>
    )
  }

  render() {

    return (
        <tr>
          {this.renderIdSection()}
          {this.renderTitreSection()}
          {this.renderAutheurSection()}
          {this.renderActionSection()}
        </tr>
    );
  }

  onEditClick() {
    this.setState({ isEditing: true});
  }
  onCancelClick() {
    this.setState({ isEditing: false});
  }
  onSaveClick(event) {
    event.preventDefault();
    const oldTitre = this.props.titre;
    const newTitre = this.refs.editInput.value;
    const newAutheur = this.refs.editInputAutheur.value;
    this.props.saveLivre(oldTitre, newTitre, newAutheur);
    this.setState({ isEditing: false});
  }
}
